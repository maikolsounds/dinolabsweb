<p align="center"><a href="https://dinolabs.dev"><img src="https://res.cloudinary.com/maikolsounds/image/upload/dpr_auto,q_auto,f_auto,w_300/v1592845017/dinolabs/dinolabs_logo_words_color"></a></p>

# Acerca de DinoLabs

DinoLabs es una agencia digital enfocada en el desarrollo de páginas web.

La Página Web esta hecha a mano, es decir, sin uso de CMS o similares.
Se aplican buenas practicas para la estructura del código tanto HTML semántico, como de Javascript Moderno.

# Tecnologías

- [Bootstrap 5](https://v5.getbootstrap.com/) - Framework de CSS
- [Cloudinary](https://cloudinary.com/) - CDN de assets como imágenes y archivos de tipografías.
- [Firebase](https://firebase.google.com/) - Hosting para la Web.
- [FontAwesome SVG Core](https://fontawesome.com/how-to-use/on-the-web/advanced/svg-javascript-core) - Proveedor de iconos para la web.
- [Laravel Mix](https://laravel-mix.com/) - Compilación de Assets.
- [SASS](https://sass-lang.com/) - Preprocesador de CSS.
- [Vue](https://vuejs.org/) - Framework de Javascript.

# Instalación:

Instalar las "dependencies" y "devDependencies" y ejecutar en el navegador.

- **cd dinolabsweb**
- **npm install** / **yarn**
- **npm run watch** / **yarn watch**

Para entornos de producción..

- **npm run prod** / **yarn prod**

Para subir a Firebase

- **firebase login**
- **firebase deploy**

## Aliados

[Synergy Business Group SAS](https://sybugroup.com/)
