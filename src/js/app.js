import Vue from 'vue';
import Toastr from 'toastr2';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { faEnvelope, faCopy } from '@fortawesome/free-regular-svg-icons';
import {
  faWhatsapp,
  faInstagram,
  faFacebookSquare,
  faTwitterSquare
} from '@fortawesome/free-brands-svg-icons';

library.add(faWhatsapp, faEnvelope, faInstagram, faFacebookSquare, faTwitterSquare, faCopy);
dom.watch();

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;

window.toastr = new Toastr();
import 'toastr2/dist/toastr.min.css';

// Get all elements width 'data-src' attribute
const images = document.querySelectorAll('[data-src]');

// On Load Page
setElements(images);

// On Window Resize
window.addEventListener('resize', () => {
  setElements(images);
});

// Set or remove the 'src' attribute
function setSrc(size, item = null) {
  if (Number(size) >= Number(size) && Number(size) <= window.innerWidth) {
    item.src = item.dataset.src;
  } else {
    item.removeAttribute('src');
  }
}

function setElements(images) {
  images.forEach(img => {
    if (typeof img.sizes === 'Object') {
      img.sizes.forEach(size => {
        setSrc(size, img);
      });
    } else {
      setSrc(img.sizes, img);
    }
  });
}

Vue.component('form-content', require('./components/FormContent').default);

new Vue({
  el: '#app'
});
